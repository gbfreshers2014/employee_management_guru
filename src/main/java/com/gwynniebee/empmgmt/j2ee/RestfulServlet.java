/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.empmgmt.j2ee;

import com.gwynniebee.rest.service.j2ee.GBRestletServlet;
import com.gwynniebee.empmgmt.restlet.EmpMgmtApplication;

/**
 * @author sarath
 */
@SuppressWarnings("serial")
public class RestfulServlet extends GBRestletServlet<EmpMgmtApplication> {
    /**
     * This is called by tomcat's start of servlet.
     */
    public RestfulServlet() {
        this(new EmpMgmtApplication());
    }

    /**
     * This is called by u+Pavan
Gmail
Images
Share
nit test which create a subclass of this class with subclass of application.
     * @param app application
     */
    public RestfulServlet(EmpMgmtApplication app) {
        super(app);
    }
}
