package com.gwynniebee.empmgmt.dao;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.empmgmt.mapper.EmployeeMapper;
import com.gwynniebee.empmgmt.pojos.Employee;

@RegisterMapper(EmployeeMapper.class)
public interface EmployeeDAO extends Transactional<EmployeeDAO> {
	
	@GetGeneratedKeys
    @SqlUpdate("insert into Employee (empId,emailId,fName,phone) values(:empId,:emailId,:fName,:phone)")
    int addEmployee(@Bind("empId") int uUid, @Bind("emailId") String emailId, @Bind("fName") String fName, @Bind("phone") int String);

	@SqlUpdate("update Employee set status=0 where empId=:empId")
	int deleteEmployee (@Bind("empId") int empId);
	
	@SqlQuery("select empId, emailId, fName, phone, status from Employee")
    List<Employee> getEmployeesDetails();
	
	void close();
}
