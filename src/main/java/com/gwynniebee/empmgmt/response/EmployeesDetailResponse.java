package com.gwynniebee.empmgmt.response;

import java.util.List;

import com.gwynniebee.empmgmt.pojos.Employee;
import com.gwynniebee.rest.common.response.AbstractResponse;

public class EmployeesDetailResponse extends AbstractResponse{
	
	List<Employee> ees;

	public List<Employee> getEes() {
		return ees;
	}

	public void setEes(List<Employee> ees) {
		this.ees = ees;
	}
		

}
